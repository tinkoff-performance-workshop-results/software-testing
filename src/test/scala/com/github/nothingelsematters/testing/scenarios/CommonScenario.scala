package com.github.nothingelsematters.testing.scenarios

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import com.github.nothingelsematters.testing.cases._

object CommonScenario {
  def apply(): ScenarioBuilder = new CommonScenario().scn
}

class CommonScenario {
  val scn: ScenarioBuilder = scenario("Common Scenario").exec(GetMainPage.getMainPage)
}
