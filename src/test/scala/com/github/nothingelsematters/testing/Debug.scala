package com.github.nothingelsematters.testing

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import ru.tinkoff.gatling.config.SimulationConfig._
import com.github.nothingelsematters.testing.scenarios.CommonScenario

class Debug extends Simulation {

  setUp(CommonScenario().inject(atOnceUsers(1)))
    .protocols(testing.httpProtocol)
    .maxDuration(testDuration)
}
